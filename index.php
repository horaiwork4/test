<?php
abstract class Sorting
{
    abstract public function sort(string $sort) : array;
}

class SortArray extends Sorting
{
    protected $arrays;
    protected $sortName = ['asc', 'desc'];

    public function __construct(array $arrays)
    {
        $this->arrays = $arrays;
    }

    public function sort(string $sort = 'asc') : array
    {
        return $this->arraySort($this->arrays, $this->hasSortName($sort));
    }

    public function hasSortName(string $name) : string
    {
        if (array_key_exists($name, array_flip($this->sortName))) {
            return $name;
        }

        return 'asc';
    }

    protected function arraySort(array $arrays, string $sort) : array
    {
        $matrix = [];
        foreach ($arrays as $keyArray => $array) {
            foreach ($array as $keyList => $list) {
                $matrix[$keyList][] = $list;
            }
        }
 
        return $this->sortBy($matrix, $sort);
    }


    private function sortBy(array $matrix, string $sort) : array
    {
        foreach ($matrix as $key => $val) {
            if ($sort === 'asc') {
                asort($matrix[$key]);
            }
            if ($sort === 'desc') {
                arsort($matrix[$key]);
            }
        }

        return $matrix;
    }
}


$arrays = [
[21,45,59,1,5],
[17,4,6,111,15],
[324,48,29,10,58],
[210,41,12,19,6],
[17,88,65,18,90],
];

$sortArrays = new SortArray($arrays);



print_r($sortArrays->sort('asc'));
print_r($sortArrays->sort('desc'));

